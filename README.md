`pdf-duplex` - Split a pdf file into two files to allow two-sided (duplex)
printing on a printer that doesn't do two-sided printing.

Version 2020-sep-08

`pdf-duplex` is a bash script; drop it in any convenient location.

Check the hash-bang line for the right bash invocation (it uses `env` so that
shouldn't be a problem, but there have been weird results with various *ix
systems).

`pdf-duplex` uses poppler utilities ( https://poppler.freedesktop.org/ or your
system's package manager), so they should be installed too.

There are no databases to install.

There are no tests to install or run (maybe there should be).

`pdf-duplex` is deployed as your basic *ix command.

rvclayton@bitbucket.org
